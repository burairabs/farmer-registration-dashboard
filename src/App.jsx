import React, { useEffect } from "react";
import { Routes, Route, useLocation } from "react-router-dom";
import Layout from "./components/Layout";
import "./css/styles.scss";

// Import pages
import Dashboard from "./pages/Dashboard";
import Farmers from "./pages/Farmers";
import Onboarding from "./pages/Onboarding";
import SignIn from "./pages/SignIn";
import SignUp from "./pages/SignUp";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import store from "./redux/store";
import { PrivateRoute, AuthRoute } from "./components/ProtectedRoute";
import "react-toastify/dist/ReactToastify.css";

function App() {
  const location = useLocation();

  useEffect(() => {
    document.querySelector("html").style.scrollBehavior = "auto";
    window.scroll({ top: 0 });
    document.querySelector("html").style.scrollBehavior = "";
  }, [location.pathname]); // triggered on route change

  return (
    <>
      <Provider store={store}>
        <ToastContainer />
        <Routes>
          <Route path="/signin" element={<AuthRoute />}>
            <Route path="/signin" element={<SignIn />} />
          </Route>
          <Route path="/" element={<PrivateRoute />}>
            <Route
              exact
              path="/"
              element={
                <Layout>
                  <Dashboard />
                </Layout>
              }
            />
          </Route>
          <Route
            exact
            path="/onboarding"
            element={
              <Layout>
                <Onboarding />
              </Layout>
            }
          />
          <Route
            exact
            path="/farmers"
            element={
              <Layout>
                <Farmers />
              </Layout>
            }
          />
          <Route exact path="/signup" element={<SignUp />} />
        </Routes>
      </Provider>
    </>
  );
}

export default App;
