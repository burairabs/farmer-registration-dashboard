import axios from "axios";
import jwt_decode from "jwt-decode";

const API_URL = "https://staging-sso.agrotrust.io/sso/api/user/byPassLogin";

const login = (values) => {
  const finalObj = {
    email: values.email,
    password: values.password,
  };

  return axios.post(API_URL, finalObj).then((response) => {
    if (response.data) {
      const user = jwt_decode(response.data.token);
      localStorage.setItem("user", JSON.stringify(user));
      return user;
    }

    return;
  });
};

const logout = () => {
  localStorage.removeItem("user");
};

export { login, logout, API_URL };
