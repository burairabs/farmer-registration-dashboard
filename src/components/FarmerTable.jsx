import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { baseUrl } from "../utils/Utils";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import FarmerIcon from "../images/farmer.png";

function FarmerTable() {
  const [farmersData, setFarmersData] = useState([]);

  useEffect(() => {
    getFarmers();
  }, []);

  const getFarmers = async () => {
    const endpoint = `${baseUrl}/api/v2/farmer`;
    try {
      let response = await axios.get(endpoint);
      if (response) {
        console.log(response.data, "ye farmer data response");
        setFarmersData(response.data);
      }
    } catch (error) {
      toast.error(`${error.message}`);
    }
  };

  return (
    <div className="col-span-full xl:col-span-8 bg-white shadow-lg rounded-sm border border-slate-200 mt-10">
      <header className="px-5 py-4 border-b border-slate-100 flex justify-between">
        <div>
          <h1 className="text-2xl mb-1 font-semibold text-gray-900">
            Farmers List
          </h1>
        </div>
        <div>
          {/* Add New button */}
          <Link to="/onboarding">
            <button className="btn bg-[#00AB55] shadow-lg shadow-green-500/50 hover:bg-[#00AB55] text-white">
              <svg
                className="w-4 h-4 fill-current opacity-50 shrink-0"
                viewBox="0 0 16 16"
              >
                <path d="M15 7H9V1c0-.6-.4-1-1-1S7 .4 7 1v6H1c-.6 0-1 .4-1 1s.4 1 1 1h6v6c0 .6.4 1 1 1s1-.4 1-1V9h6c.6 0 1-.4 1-1s-.4-1-1-1z" />
              </svg>
              <span className="hidden xs:block ml-2">Add New</span>
            </button>
          </Link>
        </div>
      </header>
      <div className="p-3">
        {/* Table */}
        <div className="overflow-x-auto">
          <table className="table-auto w-full">
            {/* Table header */}
            <thead className="text-xs uppercase text-slate-400 bg-slate-50 rounded-sm">
              <tr>
                <th className="p-2">
                  <div className="font-semibold text-left">Farmer Name</div>
                </th>
                <th className="p-2">
                  <div className="font-semibold text-center">Dob</div>
                </th>
                <th className="p-2">
                  <div className="font-semibold text-center">Village</div>
                </th>
                <th className="p-2">
                  <div className="font-semibold text-center">Taluka</div>
                </th>
                <th className="p-2">
                  <div className="font-semibold text-center">District</div>
                </th>
                <th className="p-2">
                  <div className="font-semibold text-center">State</div>
                </th>
                {/* <th className="p-2">
                  <span className="sr-only">Edit</span>
                </th> */}
              </tr>
            </thead>
            {/* Table body */}
            <tbody className="text-sm font-medium divide-y divide-slate-100">
              {/* Row */}
              {farmersData.map((person) => (
                <tr>
                  <td className="p-2">
                    <div className="flex items-center">
                      <img
                        src={FarmerIcon}
                        alt="farmer"
                        className="w-15 h-10 mr-5 rounded-full"
                      />
                      <div className="d-block">
                        <div className="text-slate-800">
                          {person?.firstName} {person?.lastName}
                        </div>
                        <div className="text-sm text-gray-500">
                          {person?.gender} {person?.phoneNumber}
                        </div>
                      </div>
                    </div>
                  </td>
                  <td className="p-2">
                    <div className="d-block">
                      <div className="text-center">{person?.dob}</div>
                      {/* <div className="text-sm text-gray-500">
                        {person.department}
                      </div> */}
                    </div>
                  </td>
                  <td className="p-2">
                    <div className="text-center">
                      {/* <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                        Active
                      </span> */}
                      {person?.address?.village}
                    </div>
                  </td>
                  <td className="p-2">
                    <div className="text-center">
                      {/* <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                        Active
                      </span> */}
                      {person?.address?.taluka}
                    </div>
                  </td>
                  <td className="p-2">
                    <div className="text-center">
                      {/* <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                        Active
                      </span> */}
                      {person?.address?.district}
                    </div>
                  </td>
                  <td className="p-2">
                    <div className="text-center">{person?.address?.state}</div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default FarmerTable;
